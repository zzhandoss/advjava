package com.secondyear.java.repository;

import com.secondyear.java.model.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface FondRepository extends CrudRepository<Fond,Long> {

    List<Fond> findAll();
}
