package com.secondyear.java.repository;

import com.secondyear.java.model.Nomenclature;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {

    List<Nomenclature> findAll();
}
