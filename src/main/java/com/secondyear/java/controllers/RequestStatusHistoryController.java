package com.secondyear.java.controllers;

import com.secondyear.java.model.RequestStatusHistory;
import com.secondyear.java.services.RequestStatusHistoryServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestStatusHistoryController {

    private final RequestStatusHistoryServices requestStatusHistoryServices;

    public RequestStatusHistoryController(RequestStatusHistoryServices requestStatusHistoryServices) {
        this.requestStatusHistoryServices = requestStatusHistoryServices;
    }

    @GetMapping("/RequestsStatusHistory")
    public ResponseEntity<?> getRequestsStatusHistory(){
        return ResponseEntity.ok ( requestStatusHistoryServices.getAll () );
    }

    @GetMapping("/RequestStatusHistory/{requestStatusHistoryId}")
    public ResponseEntity<?> getRequestStatusHistory(@PathVariable Long requestStatusHistoryId){
        return ResponseEntity.ok ( requestStatusHistoryServices.getById ( requestStatusHistoryId ) );
    }

    @PostMapping("/RequestStatusHistory")
    public ResponseEntity<?> saveRequestStatusHistory(@RequestBody RequestStatusHistory requestStatusHistory){
        return ResponseEntity.ok ( requestStatusHistoryServices.create ( requestStatusHistory ) );
    }

    @PutMapping("/RequestStatusHistory")
    public ResponseEntity<?> updateRequestStatusHistory(@RequestBody RequestStatusHistory requestStatusHistory){
        return ResponseEntity.ok ( requestStatusHistoryServices.update ( requestStatusHistory ) );
    }

    @DeleteMapping("/RequestStatusHistory/{requestStatusHistoryId}")
    public void deleteRequestStatusHistory(@PathVariable Long requestStatusHistoryId){
        requestStatusHistoryServices.delete ( requestStatusHistoryId );
    }
}
