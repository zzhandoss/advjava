package com.secondyear.java.controllers;

import com.secondyear.java.model.Location;
import com.secondyear.java.services.LocationServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {

    private final LocationServices locationServices;

    public LocationController(LocationServices locationServices) {
        this.locationServices = locationServices;
    }

    @GetMapping("/Locations")
    public ResponseEntity<?> getLocations(){
        return ResponseEntity.ok ( locationServices.getAll () );
    }

    @GetMapping("/Location/{locationId}")
    public ResponseEntity<?> getLocation(@PathVariable Long locationId){
        return ResponseEntity.ok ( locationServices.getById ( locationId ) );
    }

    @PostMapping("/Location")
    public ResponseEntity<?> saveLocation(@RequestBody Location location){
        return ResponseEntity.ok ( locationServices.create ( location ) );
    }

    @PutMapping("/Location")
    public ResponseEntity<?> updateLocation(@RequestBody Location location){
        return ResponseEntity.ok ( locationServices.update ( location ) );
    }

    @DeleteMapping("/Location/{locationId}")
    public void deleteLocation(@PathVariable Long locationId){
        locationServices.delete ( locationId );
    }
}
