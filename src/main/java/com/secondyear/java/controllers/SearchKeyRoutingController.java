package com.secondyear.java.controllers;

import com.secondyear.java.model.SearchKeyRouting;
import com.secondyear.java.services.SearchKeyRoutingServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRoutingController {

    private final SearchKeyRoutingServices searchKeyRoutingServices;

    public SearchKeyRoutingController(SearchKeyRoutingServices searchKeyRoutingServices) {
        this.searchKeyRoutingServices = searchKeyRoutingServices;
    }

    @GetMapping("/SearchKeysRouting")
    public ResponseEntity<?> getSearchKeysRouting(){
        return ResponseEntity.ok ( searchKeyRoutingServices.getAll () );
    }

    @GetMapping("/SearchKeyRouting/{searchKeyRoutingId}")
    public ResponseEntity<?> getSearchKeyRouting(@PathVariable Long searchKeyRoutingId){
        return ResponseEntity.ok ( searchKeyRoutingServices.getById ( searchKeyRoutingId ) );
    }

    @PostMapping("/SearchKeyRouting")
    public ResponseEntity<?> saveSearchKeyRouting(@RequestBody SearchKeyRouting searchKeyRouting){
        return ResponseEntity.ok ( searchKeyRoutingServices.create ( searchKeyRouting ) );
    }

    @PutMapping("/SearchKeyRouting")
    public ResponseEntity<?> updateSearchKeyRouting(@RequestBody SearchKeyRouting searchKeyRouting){
        return ResponseEntity.ok ( searchKeyRoutingServices.update ( searchKeyRouting ) );
    }

    @DeleteMapping("/SearchKeyRouting/{searchKeyRoutingId}")
    public void deleteSearchKeyRouting(@PathVariable Long searchKeyRoutingId){
        searchKeyRoutingServices.delete ( searchKeyRoutingId );
    }
}
